const jumpingOnClouds = (c) => {
    let jumps = 0; // Almacena el numbero de saltos
    let i = 0;  // Sumador que controla el ciclo while

    let arrLength = c.length; // Longitud del array

    while (true) {
        // Salta de a 2, (Si es menor que longitud del arr y si esa posicion tiene '0')
        if (i + 2 < arrLength && c[i + 2] == 0) {
            i += 2;
        } 
        // Salta de a 1, (Si es menor que longitud del arr, es decir que puede continuar saltando)
        else if (i + 1 < arrLength) {
            i++;
        }
        // Si las opciones anterior no se cumplen rompe el ciclo y devuelve "jumps"
        else {
            break;
        }
        // Suma los saltos (Ciclos efectuados)
        jumps++;
    }
    return jumps;
};

let c1 = [0, 0, 1, 0, 0, 1, 0]; // 4
let c2 = [0, 0, 0, 0, 1, 0]; //3
let c3 = [0, 1, 0, 0, 0, 1, 0]; // 3
let c4 = [0, 1, 0]; // 1

console.log("Saltos array 1: ", jumpingOnClouds(c1));
console.log("Saltos array 2: ", jumpingOnClouds(c2));
console.log("Saltos array 3: ", jumpingOnClouds(c3));
console.log("Saltos array 4: ", jumpingOnClouds(c4));