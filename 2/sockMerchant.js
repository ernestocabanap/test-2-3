const sockMerchant = (n, ar) => {
    var count = 0; // Almacena la cantidad de pares de nedua
    var socks = {}; // Almacena la cantidad del tipo de par de media

    // Primer for loop itera el array de medias
    for (var i = 0; i < n; i++) {
        // Analiza si el color actual de la media se encuentra en "socks"
        if (ar[i] in socks) {
            // Incrementa la cuenta del color por 1 y lo almacena en "socks"
            socks[ar[i]] +=  1;
        } else {
            // Si no, agrega el color y le asigna la cuenta de 1
            socks[ar[i]] = 1;
        }
    }
    // Despues de contar y almacenar los tipos de medias y cuantas hay por tipo
    // Segundo for loop itera sobre cada llave en el objeto
    for (var key in socks) {
        // Verifica que la llave del objeto existe
        if (socks.hasOwnProperty(key)) {
            /** 
             *  Almacena la cuenta
             *  La segunda parte divive por 2 para verificar si es par
             *  Si es impar, lo redondea hacia abajo y lo suma a la cuenta
             *  Ej: 3/2 = 1,5 ; count = count + 1
             */
            count += Math.floor(socks[key] / 2);
        }
    }
    return count;
};

const n = 9;
const ar = [10, 20, 20, 10, 10, 30, 50, 10, 20];
console.log("La cantidad de pares de medias que Jhon puede vender es: ", sockMerchant(n, ar));

const n2 = 7;
const ar2 = [10, 20, 10, 20, 10, 30, 20];
console.log("La cantidad de pares de medias que Jhon puede vender es: ", sockMerchant(n2, ar2));